********************************************************************************
Installation
********************************************************************************

Getting binary package
================================================================================

Binary builds are hosted at **bitbucket**:

https://bitbucket.org/restlessmonkey/lfstop/downloads

There go all the released binaries.

For example::

    lfstop-2.0.7-windows-i686.zip

    lfstop-2.0.7-linux-i686.tar.gz

The first one is for **Windows** systems, the other one is for **Linux**. They
are just archives with binaries, libraries and some data files needed to run
**LFSTop**.

Normally big software products have installer for Wwindows systems, or
distribution-specific packages for linux and other UNIX-like systems. I decided
to provide just simple binary archives.

Building from Source
================================================================================

Here it is expected that you have some experience in compiling C++ programs for
your platform.


Rationale
--------------------------------------------------------------------------------
Why to build it yourself?

* You can get 64bit binary, while LFStop binary packages are 32 bit.
* You can tune compiler for your platform/liking.
* You can disable all the debug-friendly options (which I, however, do not
  recommend to do, unless you test the program enough with your workflow)
* You join the development ofcourse!


Requirements
--------------------------------------------------------------------------------

* **C++** compiler with **c++11** support. Tested with gcc 4.8.2 (and newer).
    You can also use **MSVC Update 3** (or newer) on Windows.
* **Qt Core**, and **Qt Networking** libraries version 5.3 or higher (and also,
  **Qt Tests** lib for building tests) (TODO disable tests when no tests lib or
  just to speed up release preparation maybe)
* **CMake 3.0.0** or newer.


Invoking cmake and building
--------------------------------------------------------------------------------

Normally it will look like this::

    cmake -DCMAKE_INSTALL_PREFIX=/home/bob/lfstop ~/path/to/sources

Given it run successfully, on Linux you would normally run::

    make install

Depending on what toolchain you use on windows, you either run something like::

    mingw32-make install

Or you get **Visual Studio** solution which you build from there.
