.. _configuring-label:

********************************************************************************
Configuring
********************************************************************************

Both ``config.ini`` and ``servers/<your server name>.ini`` files have some
sections defined. Section is defined like this::

  [<section name>]

, where ``<section name>`` is the name of the section.

Every line which starts with ``#`` is ignored by program and is intended for
comments.

Normally you can just browse your config files and comments should reveal
everything you need to know. However, some options might need more
documentation, so there we go.


config.ini
================================================================================

This file contains global **LFSTop** application settings.

``settings`` section
---------------------------------------------------------------------------------

``pubstat_key`` - Pubstat ident-key which allows **LFSTop** to query some useful
data from **Live For Speed World** statistics.

``wr_update_interval`` - how often to update (in minutes) world records from
**Live For Speed World** in.


``classes`` section
---------------------------------------------------------------------------------

Here you define your classes. Syntax is just::
  <class name> <car1> <car2> ...

You can use custom cars defined in the next sections.

``cars`` section
---------------------------------------------------------------------------------
Here you define your custom cars. Syntax is::

  <car name> <lfs car> <intake restriction> <added mass>

So if you want to add a car named ``OMG`` which will be a ``XRG`` with 40%
intake restriction and 70kg added mass, you add this line::

  OMG XRG 40 70

server.ini
================================================================================

``host``
---------------------------------------------------------------------------------
**Live For Speed** host address. Can be IP address or ``localhost`` if it runs
on the same host as **LFSTop**. Domain name can work too.

``password``
---------------------------------------------------------------------------------
**Live For Speed** host admin password.

``port``
---------------------------------------------------------------------------------
**Live For Speed** host **insim** port.

``udp_port``
---------------------------------------------------------------------------------
UDP port which **LFSTop** will use to receive car positions data from **Live For
Speed**.

``command_prefix``
---------------------------------------------------------------------------------
A symbol which indicates a command from user. Can be any non-letter symbol, like
``@``, ``$``, etc.

``cars``
---------------------------------------------------------------------------------
You can put allowed cars there. Syntax is easier to understand by example.

Simple example, ``GTR`` class and ``FZ5``::

  cars GTR+FZ5

Everything but ``UF1`` and ``BF1``::

  cars ALL-UF1-BF1

You can leave it blank, so that **LFSTop** will not interfere with **Live For
Speed** server settings.

But there is a good purpose for this command. If you want to allow only
restricted cars, you can achieve that with **LFSTop**. Just put the restricted
cars or classes there.

You can have cars like this::

  FZ1 FZR 0 20
  FZ2 FZR 20 0
  FZ3 FZR 28 0

Here we have 3 cars - ``FZ1``, ``FZ2`` and ``FZ3`` which we define as ``FZR``
with some restrictions.

If we put this to ``servers/bob_server.ini``::

  cars FZ1

Then any FZR with 20 or more kilograms will be allowed on the server. In the
case of our example, all of them will count as ``FZ1``.

If we put this to ``servers/bob_server.ini``::

  cars FZ2

Then any FZR with 20% engine restriction and any amount of added mass be allowed
on the server. In the case of our example that would be ``FZ2`` and ``FZ3``.

**WARNING**: If you put restricted cars in ``cars`` option and then decide to
shutdown the **LFSTop**, you will have restrictions applied anyway. To clear
them, you can run ``!cars all`` prior to **LFSTop** shutdown, or you can restart
**Live For Speed** host.


``hlvc``
---------------------------------------------------------------------------------
What to count as clean laps.

Available options:

* ``ground`` - check if the car does not go away from the driving surface
* ``wall`` - wall hits
* ``contact`` - contacts with other cars
* ``object_hit`` - movable objects and layout object hits
* ``draft`` - driving in the slipstream of other cars

Examples::

  # check for everything but draft

  hlvc all-draft

  # self-explanatory

  ground+wall+contact


``hlvc_openconfig``
---------------------------------------------------------------------------------
Same as ``hlvc`` but for openconfig tracks.


``debug_insim_packets``
---------------------------------------------------------------------------------
If set ``true``, will dump all the packets from **Live For Speed** to
``logs/<server name>.log``.

``recent_admins_only``
---------------------------------------------------------------------------------
If set ``true``, will restrict ``!recent`` command to admins only.

``recent_limit``
---------------------------------------------------------------------------------
Number of recent drivers to save for ``!recent`` command output.

``log_admins_only``
---------------------------------------------------------------------------------
If set ``true``, will restrict ``!log`` command to admins only.

``log_limit``
---------------------------------------------------------------------------------
Number of strings to save for ``!log`` command output. Long strings are split to
2 for log output, so that number does not exactly reflect the number of recent
messages.

``data_export_enable``
---------------------------------------------------------------------------------
If set ``true``, will dump window content by user request to the path specified
in ``data_export_path`` and give the link to that file, by printing
``data_export_public_url_prefix/<file_name>`` (see further data export options)

**Example:**

Suppose you set ``data_export_path`` to ``/var/www/lfstop_export``. You need to
set ``data_export_url_prefix``. Probably that would be something like
``http://www.bob.com/lfstop_export/``.

That would work like this:

#. User presses ``Export`` button in some **LFSTop** window in the game
#. **LFSTop** writes the contents of the window to the ``/var/www/lfstop_export/<encoded time of the export>.txt``
#. **LFSTop** sends the message to the user, something like ``http://www.bob.com/lfstop_export/aiu12fuh.txt``
#. User can now visit the link and examine the data.


``data_export_path``
---------------------------------------------------------------------------------
A place where to write the files where user supposed to have acces for them
through the ``data_export_public_url_prefix``

``data_export_public_url_prefix``
---------------------------------------------------------------------------------
Link, which will be prefixed to the file name when sending user the message
after dumping the window content.

``track_vote_enable``
---------------------------------------------------------------------------------
If set ``true``, will start voting for next track after session ends.

``track_vote_time``
---------------------------------------------------------------------------------
Time (in seconds) to wait for everyone to cast a vote.

``track_vote_time_to_cancel``
---------------------------------------------------------------------------------
Time (in seconds) how soon to cancel voting if no votes have been cast.

``stints_num_to_save``
---------------------------------------------------------------------------------
Number of stints (lap-by-lap data) to save.

``stints_min_num_laps_to_save``
---------------------------------------------------------------------------------
Number of laps required to be done for a stint to be saved.

``[hlvc_overrides]`` section
---------------------------------------------------------------------------------
You can put there specific overrides for each track or layout.

**Layout example::**

  AU1_Kilimanjaro all-draft
  
Disables draft on ``AU1_Kilimanjaro`` layout.

``[setup_restrictions]`` section
---------------------------------------------------------------------------------
``forbid_traction_control`` 

If set to ``true``, will forbid traction control.

``forbid_automatic_gears`` 
If set to ``true``, will forbid automatic gears.

``clutch``
Set allowed clutch types. You can specify ``axis``, ``button`` and ``auto``.

**Example**::
  
  clutch axis+auto
  
Forbids only button clutch.

``[custom_info_commands]`` section
---------------------------------------------------------------------------------
Here you can define custom info commands. First goes command name, second is the
name of the file to display.

**Example**::

  info info.txt

