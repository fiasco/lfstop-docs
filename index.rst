.. LFSTop documentation master file, created by
   sphinx-quickstart on Tue Feb 21 21:16:43 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#################################################################################
LFSTop user guide
#################################################################################

TODO

#################################################################################
LFSTop admin guide
#################################################################################

.. toctree::
   :maxdepth: 5
   :caption: Contents:

   obtaining
   setting_up
   configuring
   file_hierarchy
   common_problems_faq
   reporting_problems

#################################################################################
Glossary
#################################################################################
TODO some terms





The project is licensed under the GNU GPL license.


.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
