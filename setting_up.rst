********************************************************************************
Setting up
********************************************************************************

Live For Speed server setup
================================================================================

You need to know the **insim port**, **admin password** and Live For Speed host
IP address (if it runs on another machine).

Example content of LFS Dedicated Server ``setup.cfg``::

    ...

    /admin=phie4On4
    /insim=29999

    ...


So ``phie4On4`` is the password which you will need to set in LFSTop server
config. ``29999`` is the port.


Setting up LFSTop
================================================================================

Unpacked binary distribution should look like this::

  ├── data
  │   └── IP2LOCATION-LITE-DB1.BIN
  ├── docs
  │   ├── ENG.txt
  │   └── Translating.txt
  ├── languages
  │   ├── CHI.txt
  │   ├── DEU.txt
  │   ├── LAV.txt
  │   ├── POL.txt
  │   ├── RUS.txt
  │   └── UKR.txt
  ├── servers
  │   └── srv1.ini.example
  ├── AUTHORS
  ├── CHANGES
  ├── config.ini.example
  ├── example_info.txt
  ├── lfstop
  ├── lfstop_airio_import
  ├── lfstop.sh
  ├── libQt5Core.so.5 -> libQt5Core.so.5.5.0
  ├── libQt5Core.so.5.5.0
  ├── libQt5Network.so.5 -> libQt5Network.so.5.5.0
  ├── libQt5Network.so.5.5.0
  ├── LICENSE
  └── README

The files we need to make LFSTop running are ``config.ini`` and
``servers/<arbitrary server name>.ini``. I suggest to make server name short, so
it doesn't take too much space in the logs.

See :ref:`file-hierarchy-label` section for more complete info about
the file chierarchy.


Configuration files
--------------------------------------------------------------------------------
In ``config.ini`` are the settings which will be used on all of the servers.

In ``servers/<server_name>.ini`` (you can have more than 1) you set up
server-specific settings.

The synthax is as follows::

  # this is a comment
  option_name <option value>


Configuring the config.ini
--------------------------------------------------------------------------------
So, rename the ``config.ini.example`` to ``config.ini``. The most useful thing
to set there is your pubstat ident-key. To retreive it, log in
http://www.lfsworld.net and then go to `Settings -> Pubstat Access
<http://www.lfsworld.net/?win=settings&whichTab=pubstat>`_

Then you can set your ``pubstat_key`` to that value you obtained from
**LFSWorld**::

  pubstat_key 85d96d31b31e4da4ac06b2f7ec22aaad


Setting up a server config
--------------------------------------------------------------------------------
Rename ``servers/srv1.ini.example`` to, for example, ``/servers/BOB_FBM_S1.ini``.


Live For Speed host address
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The host address is the IP of the server where **Live For Speed** server is
running::

  host localhost

It can be IP address or simply ``localhost`` in our case. If your **Live For
Speed** is running on another host, use the IP address of that host.


Live For Speed host admin password
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set up the password, same as in ``<Live For Speed Server
directory>/setup.cfg``::

  password phie4On4

Live For Speed host port
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Same with insim port::

  port 29999


UDP port for receiving car positions data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you need to, you can change the udp port which *LFSTop* will bind itself
to::

  udp_port 0

By default, it will try to use any free port higher than ``1024``.


LFSTop command prefix
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Command prefix is the one that LFSTop will react to. If you want to test
LFSTop alongside another insim which already uses the exclamation (!), you can
change to something else, like @, #, $, %, etc::

  command_prefix !


If you want further tuning...
--------------------------------------------------------------------------------
Refer to the :ref:`configuring-label` section for full list of **LFSTop**
options.


Starting the server on Linux
--------------------------------------------------------------------------------
Go to the server directory and run the ``lfstop`` executable::

  cd /home/bob/lfstop
  ./lfstop

To shutdown, you can press ``Ctrl+C``.


Starting the server on Windows
--------------------------------------------------------------------------------
Navigate to ``lfstop.exe`` and double-click.

To shutdown, you can press ``Ctrl+C``.


Checking the log files for any problems
--------------------------------------------------------------------------------
Normally you would be able to examine the program output for any errors or
warnings. Also, you could examine the log files in ``logs/`` directory.

``main.log`` is where all the global output goes.

``<your server name>.log`` is where server-specific message go.
